                  OLD TOWN ROAD

--> This game can be played by 2 people, who will be player one and player two. Player one starts from the bottom of the screen and player two will start from the top of the screen.
--> Player one controls his cowboy with the arrow keys and player two controls his cowboy with WASD keys. The aim of this game is to get to the opposite end while dodging charging bulls and spiky cactus. 
--> For every row of bulls crossed, the player will be assigned +10 points. For every row of cactus crossed, the player will be assigned +5 points.
--> But, time also matters....
As time keeps running, the player keeps loosing points. The amount of points lost to time depends on the number of iterations the while running loop of each player. 
--> If one player passes the level, his/her next level has bulls charging faster than the previous level.
--> If a player dies and the other player does not, the other player needs to keep playing until he/she dies.
--> In a circumstance that both the players have died, their final scores are displayed and the game restarts after a while, with both of them in level one with zero score.
--> There might be a case, where a person with lower points can be a winner. This case can happen when the player has crossed more levels than the other, but has taken significantly longer time. 