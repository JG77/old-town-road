
while True:
    import pygame
    import random
    import math
    import sys
    import time
    import os
    import configparser
    pygame.init()
    configparser = configparser.RawConfigParser()
    configFilePath = os.path.join(os.path.dirname(__file__), 'config.cfg')
    configparser.read(configFilePath)
    width = configparser.getint("basic", "width")
    height = configparser.getint("basic", "height")
    name = configparser.get("basic", "name")
    font = configparser.get("styles", "font")
    over = configparser.get("styles", "over")
    win1 = configparser.get("message", "win1")
    win2 = configparser.get("message", "win2")
    p1 = configparser.get("message", "p1")
    p2 = configparser.get("message", "p2")
    s1 = configparser.get("message", "s1")
    s2 = configparser.get("message", "s2")
    screen = pygame.display.set_mode((width, height))
    # title and icon
    pygame.display.set_caption(name)
    icon = pygame.image.load("cactus.png")
    # background = pygame.image.load("xx.png")
    pygame.display.set_icon(icon)
    player1 = True
    player2 = False
    col1 = False
    col2 = False
    player1_count = 0
    player2_count = 0
    score1 = 0
    score2 = 0
    while True:
        over_font = pygame.font.Font(font, 64)
        temp_font = pygame.font.Font(font, 32)
        score_font = pygame.font.Font(font, 16)

        def game_over_text(x, y, z, w):
            screen.fill((0, 0, 0))
            temp1 = z // 100
            temp2 = w // 100
            over_text = over_font.render(over, True, (255, 255, 255))
            screen.blit(over_text, (150, 150))
            one_text = temp_font.render(
                s1, True, (255, 255, 255))
            screen.blit(one_text, (150, 300))
            on_text = temp_font.render(
                str(int(x-temp1)), True, (255, 255, 255))
            screen.blit(on_text, (500, 300))
            two_text = temp_font.render(
                s2, True, (255, 255, 255))
            screen.blit(two_text, (150, 350))
            tw_text = temp_font.render(
                str(int(y-temp2)), True, (255, 255, 255))
            screen.blit(tw_text, (500, 350))
            win1_text = temp_font.render(
                win1, True, (255, 255, 255))
            win2_text = temp_font.render(
                win2, True, (255, 255, 255))
            if x > y:
                screen.blit(win1_text, (150, 400))
            elif y > x:
                screen.blit(win2_text, (150, 400))
            elif x == 0 and y == 0:
                if z < w:
                    screen.blit(win1_text, (150, 400))
                else:
                    screen.blit(win2_text, (150, 400))
            pygame.display.update()
            pygame.time.wait(4000)

        def score_text():
            score_text = score_font.render("score is ", True, (255, 255, 255))
            screen.blit(score_text, (0, 0))

        def score_number(x):
            score_text = score_font.render(str(int(x)), True, (255, 255, 255))
            screen.blit(score_text, (70, 0))

        def player1_text():
            score_text = score_font.render(p1, True, (255, 255, 255))
            screen.blit(score_text, (700, 0))

        def player2_text():
            score_text = score_font.render(p2, True, (255, 255, 255))
            screen.blit(score_text, (700, 0))

        def player1_win():
            score_text = temp_font.render(
                win1, True, (255, 255, 255))
            screen.blit(score_text, (150, 400))

        def player2_win():
            score_text = temp_font.render(
                win2, True, (255, 255, 255))
            screen.blit(score_text, (150, 400))
        if player1 and not col1:
            player1_count += 1
            # player icon
            playerImg = pygame.image.load("cowboy.png")
            playerX = 370
            playerY = 565
            playerX_change = 0
            playerY_change = 0

            # enemy icon
            # enemyImg = pygame.image.load("bull.png")
            # enemyX = random.randint(0, 736)
            # enemyY = random.randint(50, 100)
            # enemyX_change = 0.3
            # enemyY_change  = 0.3
            # from ene my import *
            # import pygame
            # import random
            enemyImg = pygame.image.load("bull.png")
            enemy1X = random.randint(0, 250)
            enemy1Y = 100
            enemy1X_change = 0.3 * math.pow(1.5, player1_count)
            enemy1Y_change = 0
            enemy2X = random.randint(251, 500)
            enemy2Y = 100
            enemy2X_change = 0.3 * math.pow(1.5, player1_count)
            enemy2Y_change = 0
            enemy3X = random.randint(500, 768)
            enemy3Y = 100
            enemy3X_change = 0.3 * math.pow(1.5, player1_count)
            enemy3Y_change = 0
            enemy4X = random.randint(0, 368)
            enemy4Y = 200
            enemy4X_change = 0.3 * math.pow(1.5, player1_count)
            enemy4Y_change = 0
            enemy5X = random.randint(369, 768)
            enemy5Y = 200
            enemy5X_change = 0.3 * math.pow(1.5, player1_count)
            enemy5Y_change = 0
            enemy6X = random.randint(0, 250)
            enemy6Y = 300
            enemy6X_change = 0.3 * math.pow(1.5, player1_count)
            enemy6Y_change = 0
            enemy7X = random.randint(251, 500)
            enemy7Y = 300
            enemy7X_change = 0.3 * math.pow(1.5, player1_count)
            enemy7Y_change = 0
            enemy8X = random.randint(501, 768)
            enemy8Y = 300
            enemy8X_change = 0.3 * math.pow(1.5, player1_count)
            enemy8Y_change = 0
            enemy9X = random.randint(0, 368)
            enemy9Y = 400
            enemy9X_change = 0.3 * math.pow(1.5, player1_count)
            enemy9Y_change = 0
            enemy10X = random.randint(0, 768)
            enemy10Y = 400
            enemy10X_change = 0.3 * math.pow(1.5, player1_count)
            enemy10Y_change = 0
            enemy11X = random.randint(0, 250)
            enemy11Y = 500
            enemy11X_change = 0.3 * math.pow(1.5, player1_count)
            enemy11Y_change = 0
            enemy12X = random.randint(251, 500)
            enemy12Y = 500
            enemy12X_change = 0.3 * math.pow(1.5, player1_count)
            enemy12Y_change = 0
            enemy13X = random.randint(501, 768)
            enemy13Y = 500
            enemy13X_change = 0.3 * math.pow(1.5, player1_count)
            enemy13Y_change = 0
            statImg = pygame.image.load("cactus.png")
            stat1X = random.randint(0, 184)
            stat1Y = 150
            stat2X = random.randint(185, 368)
            stat2Y = 150
            stat3X = random.randint(369, 552)
            stat3Y = 150
            stat4X = random.randint(553, 768)
            stat4Y = 150
            stat5X = random.randint(0, 184)
            stat5Y = 250
            stat6X = random.randint(185, 368)
            stat6Y = 250
            stat7X = random.randint(369, 552)
            stat7Y = 250
            stat8X = random.randint(553, 768)
            stat8Y = 250
            stat9X = random.randint(0, 184)
            stat9Y = 350
            stat10X = random.randint(185, 368)
            stat10Y = 350
            stat11X = random.randint(369, 552)
            stat11Y = 350
            stat12X = random.randint(553, 768)
            stat12Y = 350
            stat13X = random.randint(0, 184)
            stat13Y = 450
            stat14X = random.randint(185, 368)
            stat14Y = 450
            stat15X = random.randint(369, 552)
            stat15Y = 450
            stat16X = random.randint(553, 768)
            stat16Y = 450
            count1 = 0

            def iscollide(enemyX, enemyY, playerX, playerY):
                distance = math.sqrt(
                    math.pow(enemyX-playerX, 2) + math.pow(enemyY-playerY, 2))
                if distance < 30:
                    return True
                else:
                    return False
            # game loop start
            running = True
            while running:
                # screen.blit(background, (0, 0))
                screen.fill((239, 204, 162))
                count1 += 1
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        running = False
                        sys.exit()
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_LEFT:
                            # print("left")
                            playerX_change = -0.25
                        if event.key == pygame.K_RIGHT:
                            # print("right")
                            playerX_change = 0.25
                        if event.key == pygame.K_UP:
                            playerY_change = -0.25
                        if event.key == pygame.K_DOWN:
                            playerY_change = 0.25
                    if event.type == pygame.KEYUP:
                        if (event.key == pygame.K_LEFT or
                                event.key == pygame.K_RIGHT):
                            # print("key released")
                            playerX_change = 0
                        if (event.key == pygame.K_UP or
                                event.key == pygame.K_DOWN):
                            playerY_change = 0
                playerX += playerX_change
                playerY += playerY_change

                if playerX <= 0:
                    playerX = 0
                elif playerX >= 768:
                    playerX = 768
                if playerY <= 0:
                    playerY = 0
                elif playerY >= 568:
                    playerY = 568

                # enemyX += enemyX_change
                # enemyY += enemyY_change
                # from enemy_change import *
                enemy1X += enemy1X_change
                enemy1Y += enemy1Y_change
                enemy2X += enemy2X_change
                enemy2Y += enemy2Y_change
                enemy3X += enemy3X_change
                enemy3Y += enemy3Y_change
                enemy4X += enemy4X_change
                enemy4Y += enemy4Y_change
                enemy5X += enemy5X_change
                enemy5Y += enemy5Y_change
                enemy6X += enemy6X_change
                enemy6Y += enemy6Y_change
                enemy7X += enemy7X_change
                enemy7Y += enemy7Y_change
                enemy8X += enemy8X_change
                enemy8Y += enemy8Y_change
                enemy9X += enemy9X_change
                enemy9Y += enemy9Y_change
                enemy10X += enemy10X_change
                enemy10Y += enemy10Y_change
                enemy11X += enemy11X_change
                enemy11Y += enemy11Y_change
                enemy12X += enemy12X_change
                enemy12Y += enemy12Y_change
                enemy13X += enemy13X_change
                enemy13Y += enemy13Y_change

                # from enemy_cond import *
                if enemy1X <= 0:
                    enemy1X = 0
                    enemy1X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy1X >= 768:
                    enemy1X = 768
                    enemy1X = 0
                    enemy1X_change = 0.3 * math.pow(1.5, player1_count)
                if enemy2X <= 0:
                    enemy2X = 0
                    enemy2X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy2X >= 768:
                    enemy2X = 768
                    enemy2X = 0
                    enemy2X_change = 0.3 * math.pow(1.5, player1_count)
                if enemy3X <= 0:
                    enemy3X = 0
                    enemy3X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy3X >= 768:
                    enemy3X = 768
                    enemy3X = 0
                    enemy3X_change = 0.3 * math.pow(1.5, player1_count)
                if enemy4X <= 0:
                    enemy4X = 0
                    enemy4X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy4X >= 768:
                    enemy4X = 768
                    enemy4X = 0
                    enemy4X_change = 0.3 * math.pow(1.5, player1_count)
                if enemy5X <= 0:
                    enemy5X = 0
                    enemy5X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy5X >= 768:
                    enemy5X = 768
                    enemy5X = 0
                    enemy5X_change = 0.3 * math.pow(1.5, player1_count)
                if enemy6X <= 0:
                    enemy6X = 0
                    enemy6X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy6X >= 768:
                    enemy6X = 768
                    enemy6X = 0
                    enemy6X_change = 0.3 * math.pow(1.5, player1_count)
                if enemy7X <= 0:
                    enemy7X = 0
                    enemy7X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy7X >= 768:
                    enemy7X = 768
                    enemy7X = 0
                    enemy7X_change = 0.3 * math.pow(1.5, player1_count)
                if enemy8X <= 0:
                    enemy8X = 0
                    enemy8X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy8X >= 768:
                    enemy8X = 768
                    enemy8X = 0
                    enemy8X_change = 0.3 * math.pow(1.5, player1_count)
                if enemy9X <= 0:
                    enemy9X = 0
                    enemy9X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy9X >= 768:
                    enemy9X = 768
                    enemy9X = 0
                    enemy9X_change = 0.3 * math.pow(1.5, player1_count)
                if enemy10X <= 0:
                    enemy10X = 0
                    enemy10X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy10X >= 768:
                    enemy10X = 768
                    enemy10X = 0
                    enemy10X_change = 0.3 * math.pow(1.5, player1_count)
                if enemy11X <= 0:
                    enemy11X = 0
                    enemy11X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy11X >= 768:
                    enemy11X = 768
                    enemy11X = 0
                    enemy11X_change = 0.3 * math.pow(1.5, player1_count)
                if enemy12X <= 0:
                    enemy12X = 0
                    enemy12X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy12X >= 768:
                    enemy12X = 768
                    enemy12X = 0
                    enemy12X_change = 0.3 * math.pow(1.5, player1_count)
                if enemy13X <= 0:
                    enemy13X = 0
                    enemy13X_change = 0.3 * math.pow(1.5, player1_count)
                elif enemy13X >= 768:
                    enemy13X = 768
                    enemy13X = 0
                    enemy13X_change = 0.3 * math.pow(1.5, player1_count)
                # collisons
                # from collisions import *
                collision1 = iscollide(enemy1X, enemy1Y, playerX, playerY)
                if collision1:
                    col1 = True
                    player2 = True
                    score1 += 60
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                collision2 = iscollide(enemy2X, enemy2Y, playerX, playerY)
                if collision2:
                    col1 = True
                    player2 = True
                    score1 += 60
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                collision3 = iscollide(enemy3X, enemy3Y, playerX, playerY)
                if collision3:
                    col1 = True
                    player2 = True
                    score1 += 60
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                collision4 = iscollide(enemy4X, enemy4Y, playerX, playerY)
                if collision4:
                    col1 = True
                    player2 = True
                    score1 += 45
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                collision5 = iscollide(enemy5X, enemy5Y, playerX, playerY)
                if collision5:
                    col1 = True
                    player2 = True
                    score1 += 45
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                collision6 = iscollide(enemy6X, enemy6Y, playerX, playerY)
                if collision6:
                    col1 = True
                    player2 = True
                    score1 += 30
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                collision7 = iscollide(enemy7X, enemy7Y, playerX, playerY)
                if collision7:
                    col1 = True
                    player2 = True
                    score1 += 30
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                collision8 = iscollide(enemy8X, enemy8Y, playerX, playerY)
                if collision8:
                    col1 = True
                    player2 = True
                    score1 += 30
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                collision9 = iscollide(enemy9X, enemy9Y, playerX, playerY)
                if collision9:
                    col1 = True
                    player2 = True
                    score1 += 15
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                collision10 = iscollide(enemy10X, enemy10Y, playerX, playerY)
                if collision10:
                    col1 = True
                    player2 = True
                    score1 += 15
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                collision11 = iscollide(enemy11X, enemy11Y, playerX, playerY)
                if collision11:
                    col1 = True
                    player2 = True
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                collision12 = iscollide(enemy12X, enemy12Y, playerX, playerY)
                if collision12:
                    col1 = True
                    player2 = True
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                collision13 = iscollide(enemy13X, enemy13Y, playerX, playerY)
                if collision13:
                    col1 = True
                    player2 = True
                    player1_count += -1
                    print("collision")
                    break
                    # quit()
                coll1 = iscollide(stat1X, stat1Y, playerX, playerY)
                if coll1:
                    col1 = True
                    player2 = True
                    score1 += 55
                    player1_count += -1
                    print("collision")
                    break
                coll2 = iscollide(stat2X, stat2Y, playerX, playerY)
                if coll2:
                    col1 = True
                    player2 = True
                    score1 += 55
                    player1_count += -1
                    print("collision")
                    break
                coll3 = iscollide(stat3X, stat3Y, playerX, playerY)
                if coll3:
                    col1 = True
                    player2 = True
                    score1 += 55
                    player1_count += -1
                    print("collision")
                    break
                coll4 = iscollide(stat4X, stat4Y, playerX, playerY)
                if coll4:
                    col1 = True
                    player2 = True
                    score1 += 55
                    player1_count += -1
                    print("collision")
                    break
                coll5 = iscollide(stat5X, stat5Y, playerX, playerY)
                if coll5:
                    col1 = True
                    player2 = True
                    score1 += 40
                    player1_count += -1
                    print("collision")
                    break
                coll6 = iscollide(stat6X, stat6Y, playerX, playerY)
                if coll6:
                    col1 = True
                    player2 = True
                    score1 += 40
                    player1_count += -1
                    print("collision")
                    break
                coll7 = iscollide(stat7X, stat7Y, playerX, playerY)
                if coll7:
                    col1 = True
                    player2 = True
                    score1 += 40
                    player1_count += -1
                    print("collision")
                    break
                coll8 = iscollide(stat8X, stat8Y, playerX, playerY)
                if coll8:
                    col1 = True
                    player2 = True
                    score1 += 40
                    player1_count += -1
                    print("collision")
                    break
                coll9 = iscollide(stat9X, stat9Y, playerX, playerY)
                if coll9:
                    col1 = True
                    player2 = True
                    score1 += 25
                    player1_count += -1
                    print("collision")
                    break
                coll10 = iscollide(stat10X, stat10Y, playerX, playerY)
                if coll10:
                    col1 = True
                    player2 = True
                    score1 += 25
                    player1_count += -1
                    print("collision")
                    break
                coll11 = iscollide(stat11X, stat11Y, playerX, playerY)
                if coll11:
                    col1 = True
                    player2 = True
                    score1 += 25
                    player1_count += -1
                    print("collision")
                    break
                coll12 = iscollide(stat12X, stat12Y, playerX, playerY)
                if coll12:
                    col1 = True
                    player2 = True
                    score1 += 25
                    player1_count += -1
                    print("collision")
                    break
                coll13 = iscollide(stat13X, stat13Y, playerX, playerY)
                if coll13:
                    col1 = True
                    player2 = True
                    score1 += 10
                    player1_count += -1
                    print("collision")
                    break
                coll14 = iscollide(stat14X, stat14Y, playerX, playerY)
                if coll14:
                    col1 = True
                    player2 = True
                    score1 += 10
                    player1_count += -1
                    print("collision")
                    break
                coll15 = iscollide(stat15X, stat15Y, playerX, playerY)
                if coll15:
                    col1 = True
                    player2 = True
                    score1 += 10
                    player1_count += -1
                    print("collision")
                    break
                coll16 = iscollide(stat16X, stat16Y, playerX, playerY)
                if coll16:
                    col1 = True
                    player2 = True
                    score1 += 10
                    player1_count += -1
                    print("collision")
                    break
                screen.blit(playerImg, (playerX, playerY))
                screen.blit(enemyImg, (enemy1X, enemy1Y))
                screen.blit(enemyImg, (enemy2X, enemy2Y))
                screen.blit(enemyImg, (enemy3X, enemy3Y))
                screen.blit(enemyImg, (enemy4X, enemy4Y))
                screen.blit(enemyImg, (enemy5X, enemy5Y))
                screen.blit(enemyImg, (enemy6X, enemy6Y))
                screen.blit(enemyImg, (enemy7X, enemy7Y))
                screen.blit(enemyImg, (enemy8X, enemy8Y))
                screen.blit(enemyImg, (enemy9X, enemy9Y))
                screen.blit(enemyImg, (enemy10X, enemy10Y))
                screen.blit(enemyImg, (enemy11X, enemy11Y))
                screen.blit(enemyImg, (enemy12X, enemy12Y))
                screen.blit(enemyImg, (enemy13X, enemy13Y))
                screen.blit(statImg, (stat1X, stat1Y))
                screen.blit(statImg, (stat2X, stat2Y))
                screen.blit(statImg, (stat3X, stat3Y))
                screen.blit(statImg, (stat4X, stat4Y))
                screen.blit(statImg, (stat5X, stat5Y))
                screen.blit(statImg, (stat6X, stat6Y))
                screen.blit(statImg, (stat7X, stat7Y))
                screen.blit(statImg, (stat8X, stat8Y))
                screen.blit(statImg, (stat9X, stat9Y))
                screen.blit(statImg, (stat10X, stat10Y))
                screen.blit(statImg, (stat11X, stat11Y))
                screen.blit(statImg, (stat12X, stat12Y))
                screen.blit(statImg, (stat13X, stat13Y))
                screen.blit(statImg, (stat14X, stat14Y))
                screen.blit(statImg, (stat15X, stat15Y))
                screen.blit(statImg, (stat16X, stat16Y))
                score_text()
                score_number(score1)
                player1_text()
                pygame.display.update()
                if playerY <= 50 and not col2:
                    player2 = True
                    player1 = False
                    score1 += 70
                    break
                elif playerY <= 50 and col2:
                    player2 = False
                    player1 = True
                    score1 += 70
                    break
            # game loop end

        if player2 and not col2:
            player2_count += 1
            playerImg = pygame.image.load("cowboy1.png")
            playerX = 370
            playerY = 50
            playerX_change = 0
            playerY_change = 0

            # enemy icon
            # enemyImg = pygame.image.load("robber.png")
            # enemyX = random.randint(0, 736)
            # enemyY = random.randint(50, 100)
            # enemyX_change = 0.3
            # enemyY_change  = 0.3
            # from ene my import *
            # import pygame
            # import random
            enemyImg = pygame.image.load("bull.png")
            enemy1X = random.randint(0, 250)
            enemy1Y = 100
            enemy1X_change = 0.3 * math.pow(1.5, player2_count)
            enemy1Y_change = 0
            enemy2X = random.randint(251, 500)
            enemy2Y = 100
            enemy2X_change = 0.3 * math.pow(1.5, player2_count)
            enemy2Y_change = 0
            enemy3X = random.randint(500, 768)
            enemy3Y = 100
            enemy3X_change = 0.3 * math.pow(1.5, player2_count)
            enemy3Y_change = 0
            enemy4X = random.randint(0, 368)
            enemy4Y = 200
            enemy4X_change = 0.3 * math.pow(1.5, player2_count)
            enemy4Y_change = 0
            enemy5X = random.randint(369, 768)
            enemy5Y = 200
            enemy5X_change = 0.3 * math.pow(1.5, player2_count)
            enemy5Y_change = 0
            enemy6X = random.randint(0, 250)
            enemy6Y = 300
            enemy6X_change = 0.3 * math.pow(1.5, player2_count)
            enemy6Y_change = 0
            enemy7X = random.randint(251, 500)
            enemy7Y = 300
            enemy7X_change = 0.3 * math.pow(1.5, player2_count)
            enemy7Y_change = 0
            enemy8X = random.randint(501, 768)
            enemy8Y = 300
            enemy8X_change = 0.3 * math.pow(1.5, player2_count)
            enemy8Y_change = 0
            enemy9X = random.randint(0, 368)
            enemy9Y = 400
            enemy9X_change = 0.3 * math.pow(1.5, player2_count)
            enemy9Y_change = 0
            enemy10X = random.randint(369, 768)
            enemy10Y = 400
            enemy10X_change = 0.3 * math.pow(1.5, player2_count)
            enemy10Y_change = 0
            enemy11X = random.randint(0, 250)
            enemy11Y = 500
            enemy11X_change = 0.3 * math.pow(1.5, player2_count)
            enemy11Y_change = 0
            enemy12X = random.randint(251, 500)
            enemy12Y = 500
            enemy12X_change = 0.3 * math.pow(1.5, player2_count)
            enemy12Y_change = 0
            enemy13X = random.randint(501, 768)
            enemy13Y = 500
            enemy13X_change = 0.3 * math.pow(1.5, player2_count)
            enemy13Y_change = 0
            count2 = 0

            def iscollide(enemyX, enemyY, playerX, playerY):
                distance = math.sqrt(
                    math.pow(enemyX-playerX, 2) + math.pow(enemyY-playerY, 2))
                if distance < 30:
                    return True
                else:
                    return False
            # game loop start
            running = True
            while running:
                count2 += 1
                # screen.blit(background, (0, 0))
                screen.fill((239, 204, 162))
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        running = False
                        sys.exit()
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_a:
                            # print("left")
                            playerX_change = -0.25
                        if event.key == pygame.K_d:
                            # print("right")
                            playerX_change = 0.25
                        if event.key == pygame.K_w:
                            playerY_change = -0.25
                        if event.key == pygame.K_s:
                            playerY_change = 0.25
                    if event.type == pygame.KEYUP:
                        if event.key == pygame.K_a or event.key == pygame.K_d:
                            # print("key released")
                            playerX_change = 0
                        if event.key == pygame.K_w or event.key == pygame.K_s:
                            playerY_change = 0
                playerX += playerX_change
                playerY += playerY_change

                if playerX <= 0:
                    playerX = 0
                elif playerX >= 768:
                    playerX = 768
                if playerY <= 35:
                    playerY = 35
                elif playerY >= 568:
                    playerY = 568

                # enemyX += enemyX_change
                # enemyY += enemyY_change
                # from enemy_change import *
                enemy1X += enemy1X_change
                enemy1Y += enemy1Y_change
                enemy2X += enemy2X_change
                enemy2Y += enemy2Y_change
                enemy3X += enemy3X_change
                enemy3Y += enemy3Y_change
                enemy4X += enemy4X_change
                enemy4Y += enemy4Y_change
                enemy5X += enemy5X_change
                enemy5Y += enemy5Y_change
                enemy6X += enemy6X_change
                enemy6Y += enemy6Y_change
                enemy7X += enemy7X_change
                enemy7Y += enemy7Y_change
                enemy8X += enemy8X_change
                enemy8Y += enemy8Y_change
                enemy9X += enemy9X_change
                enemy9Y += enemy9Y_change
                enemy10X += enemy10X_change
                enemy10Y += enemy10Y_change
                enemy11X += enemy11X_change
                enemy11Y += enemy11Y_change
                enemy12X += enemy12X_change
                enemy12Y += enemy12Y_change
                enemy13X += enemy13X_change
                enemy13Y += enemy13Y_change
                # from enemy_cond import *
                if enemy1X <= 0:
                    enemy1X = 0
                    enemy1X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy1X >= 768:
                    enemy1X = 768
                    enemy1X = 0
                    enemy1X_change = 0.3 * math.pow(1.5, player2_count)
                if enemy2X <= 0:
                    enemy2X = 0
                    enemy2X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy2X >= 768:
                    enemy2X = 768
                    enemy2X = 0
                    enemy2X_change = 0.3 * math.pow(1.5, player2_count)
                if enemy3X <= 0:
                    enemy3X = 0
                    enemy3X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy3X >= 768:
                    enemy3X = 768
                    enemy3X = 0
                    enemy3X_change = 0.3 * math.pow(1.5, player2_count)
                if enemy4X <= 0:
                    enemy4X = 0
                    enemy4X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy4X >= 768:
                    enemy4X = 768
                    enemy4X = 0
                    enemy4X_change = 0.3 * math.pow(1.5, player2_count)
                if enemy5X <= 0:
                    enemy5X = 0
                    enemy5X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy5X >= 768:
                    enemy5X = 768
                    enemy5X = 0
                    enemy5X_change = 0.3 * math.pow(1.5, player2_count)
                if enemy6X <= 0:
                    enemy6X = 0
                    enemy6X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy6X >= 768:
                    enemy6X = 768
                    enemy6X = 0
                    enemy6X_change = 0.3 * math.pow(1.5, player2_count)
                if enemy7X <= 0:
                    enemy7X = 0
                    enemy7X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy7X >= 768:
                    enemy7X = 768
                    enemy7X = 0
                    enemy7X_change = 0.3 * math.pow(1.5, player2_count)
                if enemy8X <= 0:
                    enemy8X = 0
                    enemy8X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy8X >= 768:
                    enemy8X = 768
                    enemy8X = 0
                    enemy8X_change = 0.3 * math.pow(1.5, player2_count)
                if enemy9X <= 0:
                    enemy9X = 0
                    enemy9X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy9X >= 768:
                    enemy9X = 768
                    enemy9X = 0
                    enemy9X_change = 0.3 * math.pow(1.5, player2_count)
                if enemy10X <= 0:
                    enemy10X = 0
                    enemy10X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy10X >= 768:
                    enemy10X = 768
                    enemy10X = 0
                    enemy10X_change = 0.3 * math.pow(1.5, player2_count)
                if enemy11X <= 0:
                    enemy11X = 0
                    enemy11X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy11X >= 768:
                    enemy11X = 768
                    enemy11X = 0
                    enemy11X_change = 0.3 * math.pow(1.5, player2_count)
                if enemy12X <= 0:
                    enemy12X = 0
                    enemy12X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy12X >= 768:
                    enemy12X = 768
                    enemy12X = 0
                    enemy12X_change = 0.3 * math.pow(1.5, player2_count)
                if enemy13X <= 0:
                    enemy13X = 0
                    enemy13X_change = 0.3 * math.pow(1.5, player2_count)
                elif enemy13X >= 768:
                    enemy13X = 768
                    enemy13X = 0
                    enemy13X_change = 0.3 * math.pow(1.5, player2_count)
                # collisons
                # from collisions import *
                collision1 = iscollide(enemy1X, enemy1Y, playerX, playerY)
                if collision1:
                    col2 = True
                    player1 = True
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                collision2 = iscollide(enemy2X, enemy2Y, playerX, playerY)
                if collision2:
                    col2 = True
                    player1 = True
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                collision3 = iscollide(enemy3X, enemy3Y, playerX, playerY)
                if collision3:
                    col2 = True
                    player1 = True
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                collision4 = iscollide(enemy4X, enemy4Y, playerX, playerY)
                if collision4:
                    col2 = True
                    player1 = True
                    score2 += 15
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                collision5 = iscollide(enemy5X, enemy5Y, playerX, playerY)
                if collision5:
                    col2 = True
                    player1 = True
                    score2 += 15
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                collision6 = iscollide(enemy6X, enemy6Y, playerX, playerY)
                if collision6:
                    col2 = True
                    player1 = True
                    score2 += 30
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                collision7 = iscollide(enemy7X, enemy7Y, playerX, playerY)
                if collision7:
                    col2 = True
                    player1 = True
                    score2 += 30
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                collision8 = iscollide(enemy8X, enemy8Y, playerX, playerY)
                if collision8:
                    col2 = True
                    player1 = True
                    score2 += 30
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                collision9 = iscollide(enemy9X, enemy9Y, playerX, playerY)
                if collision9:
                    col2 = True
                    player1 = True
                    score2 += 45
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                collision10 = iscollide(enemy10X, enemy10Y, playerX, playerY)
                if collision10:
                    col2 = True
                    player1 = True
                    score2 += 45
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                collision11 = iscollide(enemy11X, enemy11Y, playerX, playerY)
                if collision11:
                    col2 = True
                    player1 = True
                    score2 += 60
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                collision12 = iscollide(enemy12X, enemy12Y, playerX, playerY)
                if collision12:
                    col2 = True
                    player1 = True
                    score2 += 60
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                collision13 = iscollide(enemy13X, enemy13Y, playerX, playerY)
                if collision13:
                    col2 = True
                    player1 = True
                    score2 += 60
                    player2_count += -1
                    print("collision")
                    break
                    # quit()
                coll1 = iscollide(stat1X, stat1Y, playerX, playerY)
                if coll1:
                    col2 = True
                    player2 = True
                    score2 += 10
                    player1_count += -1
                    print("collision")
                    break
                coll2 = iscollide(stat2X, stat2Y, playerX, playerY)
                if coll2:
                    col2 = True
                    player2 = True
                    score2 += 10
                    player1_count += -1
                    print("collision")
                    break
                coll3 = iscollide(stat3X, stat3Y, playerX, playerY)
                if coll3:
                    col2 = True
                    player2 = True
                    score2 += 10
                    player1_count += -1
                    print("collision")
                    break
                coll4 = iscollide(stat4X, stat4Y, playerX, playerY)
                if coll4:
                    col2 = True
                    player2 = True
                    score2 += 10
                    player1_count += -1
                    print("collision")
                    break
                coll5 = iscollide(stat5X, stat5Y, playerX, playerY)
                if coll5:
                    col2 = True
                    player2 = True
                    score2 += 25
                    player1_count += -1
                    print("collision")
                    break
                coll6 = iscollide(stat6X, stat6Y, playerX, playerY)
                if coll6:
                    col2 = True
                    player2 = True
                    score2 += 25
                    player1_count += -1
                    print("collision")
                    break
                coll7 = iscollide(stat7X, stat7Y, playerX, playerY)
                if coll7:
                    col2 = True
                    player2 = True
                    score2 += 25
                    player1_count += -1
                    print("collision")
                    break
                coll8 = iscollide(stat8X, stat8Y, playerX, playerY)
                if coll8:
                    col2 = True
                    player2 = True
                    score2 += 25
                    player1_count += -1
                    print("collision")
                    break
                coll9 = iscollide(stat9X, stat9Y, playerX, playerY)
                if coll9:
                    col2 = True
                    player2 = True
                    score2 += 40
                    player1_count += -1
                    print("collision")
                    break
                coll10 = iscollide(stat10X, stat10Y, playerX, playerY)
                if coll10:
                    col2 = True
                    player2 = True
                    score2 += 40
                    player1_count += -1
                    print("collision")
                    break
                coll11 = iscollide(stat11X, stat11Y, playerX, playerY)
                if coll11:
                    col2 = True
                    player2 = True
                    score2 += 40
                    player1_count += -1
                    print("collision")
                    break
                coll12 = iscollide(stat12X, stat12Y, playerX, playerY)
                if coll12:
                    col2 = True
                    player2 = True
                    score2 += 40
                    player1_count += -1
                    print("collision")
                    break
                coll13 = iscollide(stat13X, stat13Y, playerX, playerY)
                if coll13:
                    col2 = True
                    player2 = True
                    score2 += 55
                    player1_count += -1
                    print("collision")
                    break
                coll14 = iscollide(stat14X, stat14Y, playerX, playerY)
                if coll14:
                    col2 = True
                    player2 = True
                    score2 += 55
                    player1_count += -1
                    print("collision")
                    break
                coll15 = iscollide(stat15X, stat15Y, playerX, playerY)
                if coll15:
                    col2 = True
                    player2 = True
                    score2 += 55
                    player1_count += -1
                    print("collision")
                    break
                coll16 = iscollide(stat16X, stat16Y, playerX, playerY)
                if coll16:
                    col2 = True
                    player2 = True
                    score2 += 55
                    player1_count += -1
                    print("collision")
                    break
                screen.blit(playerImg, (playerX, playerY))
                screen.blit(enemyImg, (enemy1X, enemy1Y))
                screen.blit(enemyImg, (enemy2X, enemy2Y))
                screen.blit(enemyImg, (enemy3X, enemy3Y))
                screen.blit(enemyImg, (enemy4X, enemy4Y))
                screen.blit(enemyImg, (enemy5X, enemy5Y))
                screen.blit(enemyImg, (enemy6X, enemy6Y))
                screen.blit(enemyImg, (enemy7X, enemy7Y))
                screen.blit(enemyImg, (enemy8X, enemy8Y))
                screen.blit(enemyImg, (enemy9X, enemy9Y))
                screen.blit(enemyImg, (enemy10X, enemy10Y))
                screen.blit(enemyImg, (enemy11X, enemy11Y))
                screen.blit(enemyImg, (enemy12X, enemy12Y))
                screen.blit(enemyImg, (enemy13X, enemy13Y))
                screen.blit(statImg, (stat1X, stat1Y))
                screen.blit(statImg, (stat2X, stat2Y))
                screen.blit(statImg, (stat3X, stat3Y))
                screen.blit(statImg, (stat4X, stat4Y))
                screen.blit(statImg, (stat5X, stat5Y))
                screen.blit(statImg, (stat6X, stat6Y))
                screen.blit(statImg, (stat7X, stat7Y))
                screen.blit(statImg, (stat8X, stat8Y))
                screen.blit(statImg, (stat9X, stat9Y))
                screen.blit(statImg, (stat10X, stat10Y))
                screen.blit(statImg, (stat11X, stat11Y))
                screen.blit(statImg, (stat12X, stat12Y))
                screen.blit(statImg, (stat13X, stat13Y))
                screen.blit(statImg, (stat14X, stat14Y))
                screen.blit(statImg, (stat15X, stat15Y))
                screen.blit(statImg, (stat16X, stat16Y))
                score_text()
                score_number(score2)
                player2_text()
                pygame.display.update()
                if playerY >= 565 and not col1:
                    player1 = True
                    player2 = False
                    score2 += 70
                    print("ok")
                    break
                elif playerY >= 565 and col1:
                    player1 = False
                    player2 = True
                    score2 += 70
                    print("no")
                    break
        if col1 and col2:
            print(player1_count)
            print(player2_count)
            print(count1)
            print(count2)
            print(score1)
            print(score2)
            game_over_text(score1, score2, count1, count2)
            break
            # game loop end
